package com.dcits.business.server.dubbo;

import com.dcits.business.server.MonitoringInfo;

public class DubboMonitoringInfo  extends MonitoringInfo {
	/**
	 * 线程池状态  OK为正常
	 */
	private String threadStatus;
	/**
	 * 线程最大维护数量
	 */
	private String threadMaxCount;
	/**
	 * 线程最小维护数量
	 */
	private String threadCoreCount;
	/**
	 * 历史最大线程使用
	 */
	private String threadLargestCount;
	/**
	 * 当前活跃线程数
	 */
	private String threadActiveCount;
	/**
	 * 线程任务数
	 */
	private String threadTaskCount;
	
	/**
	 * 内存使用状态
	 */
	private String memoryStatus;
	/**
	 * 最大内存
	 */
	private String memoryMax;
	/**
	 * 全部内存
	 */
	private String memoryTotal;
	/**
	 * 使用内存
	 */
	private String memoryUsed;
	/**
	 * 空闲内存
	 */
	private String memoryFree;
	/**
	 * 内存使用百分比
	 */
	private String memoryUsedPercent;
	
	/**
	 * Load状态
	 */
	private String loadStatus;
	/**
	 * Server状态
	 */
	private String serverStatus;
	/**
	 * Registry状态
	 */
	private String registryStatus;
	
	/**
	 * spring状态
	 */
	private String springStatus;
	/**
	 * 汇总状态
	 */
	private String summaryStatus;
	
	
	public void setSpringStatus(String springStatus) {
		this.springStatus = springStatus;
	}
	
	public String getSpringStatus() {
		return springStatus;
	}
	
	public String getThreadStatus() {
		return threadStatus;
	}
	public void setThreadStatus(String threadStatus) {
		this.threadStatus = threadStatus;
	}
	public String getThreadMaxCount() {
		return threadMaxCount;
	}
	public void setThreadMaxCount(String threadMaxCount) {
		this.threadMaxCount = threadMaxCount;
	}
	public String getThreadCoreCount() {
		return threadCoreCount;
	}
	public void setThreadCoreCount(String threadCoreCount) {
		this.threadCoreCount = threadCoreCount;
	}
	public String getThreadLargestCount() {
		return threadLargestCount;
	}
	public void setThreadLargestCount(String threadLargestCount) {
		this.threadLargestCount = threadLargestCount;
	}
	public String getThreadActiveCount() {
		return threadActiveCount;
	}
	public void setThreadActiveCount(String threadActiveCount) {
		this.threadActiveCount = threadActiveCount;
	}
	public String getThreadTaskCount() {
		return threadTaskCount;
	}
	public void setThreadTaskCount(String threadTaskCount) {
		this.threadTaskCount = threadTaskCount;
	}
	public String getMemoryStatus() {
		return memoryStatus;
	}
	public void setMemoryStatus(String memoryStatus) {
		this.memoryStatus = memoryStatus;
	}
	public String getMemoryMax() {
		return memoryMax;
	}
	public void setMemoryMax(String memoryMax) {
		this.memoryMax = memoryMax;
	}
	public String getMemoryTotal() {
		return memoryTotal;
	}
	public void setMemoryTotal(String memoryTotal) {
		this.memoryTotal = memoryTotal;
	}
	public String getMemoryUsed() {
		return memoryUsed;
	}
	public void setMemoryUsed(String memoryUsed) {
		this.memoryUsed = memoryUsed;
	}
	public String getMemoryFree() {
		return memoryFree;
	}
	public void setMemoryFree(String memoryFree) {
		this.memoryFree = memoryFree;
	}
	public String getMemoryUsedPercent() {
		return memoryUsedPercent;
	}
	public void setMemoryUsedPercent(String memoryUsedPercent) {
		this.memoryUsedPercent = memoryUsedPercent;
	}
	public String getLoadStatus() {
		return loadStatus;
	}
	public void setLoadStatus(String loadStatus) {
		this.loadStatus = loadStatus;
	}
	public String getServerStatus() {
		return serverStatus;
	}
	public void setServerStatus(String serverStatus) {
		this.serverStatus = serverStatus;
	}
	public String getRegistryStatus() {
		return registryStatus;
	}
	public void setRegistryStatus(String registryStatus) {
		this.registryStatus = registryStatus;
	}
	public String getSummaryStatus() {
		return summaryStatus;
	}
	public void setSummaryStatus(String summaryStatus) {
		this.summaryStatus = summaryStatus;
	}
	
	
	
}
